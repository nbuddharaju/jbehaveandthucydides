package usersteps;

import pages.NZQAHomePage;
import pages.NZQAOverviewPage;
import net.thucydides.core.steps.ScenarioSteps;

public class UserSteps extends ScenarioSteps {

	private static final long serialVersionUID = 1L;

	public NZQAHomePage getHomePage() {
		return getPages().get(NZQAHomePage.class);
	}

	public NZQAOverviewPage getOverviewPage() {
		return getPages().get(NZQAOverviewPage.class);
	}

	public void openHomePage() {
		getHomePage().open();
	}

	public void openOverviewPage(String number) {
		getOverviewPage().open("qualNumber", new String[] { number });
	}

	public String checkForNZQAPageText() {
		return getHomePage().getNZQAText();
	}

	public String checkForQualTitle() {
		return getOverviewPage().getqualTitle();
	}

}
