package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.findby.FindBy;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("http://www.nzqa.govt.nz/")
public class NZQAHomePage extends PageObject {
	 
	 @FindBy(css="#newsInfoHome > div.homeInfo.col.infoFor > h2")
	 WebElement titleNZQA;
		
	
	public NZQAHomePage(WebDriver driver)
	{
		super(driver);
		
	}
	
	public String getNZQAText() {
		return titleNZQA.getText();
	}

}
