package pages;

import net.thucydides.core.pages.PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.NamedUrl;
import net.thucydides.core.annotations.NamedUrls;
import net.thucydides.core.annotations.findby.FindBy;

@DefaultUrl("http://www.nzqa.govt.nz/nzqf/search")
@NamedUrls({
@NamedUrl(name = "qualNumber", url = "/viewQualification.do?selectedItemKey={1}")
})
public class NZQAOverviewPage extends PageObject {
	
	 @FindBy(css="#mainPage > div.noLinkInfo > table > tbody > tr:nth-child(1) > td:nth-child(3)")
	 WebElement qualTitle;
	 
	public NZQAOverviewPage(WebDriver driver)
	{
		super(driver);
		
	}
	public String getqualTitle() {
		return qualTitle.getText();
	}
	

}
