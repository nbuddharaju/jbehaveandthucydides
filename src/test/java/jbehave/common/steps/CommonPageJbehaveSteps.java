package jbehave.common.steps;

import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.AfterStories;
import org.openqa.selenium.WebDriver;
import usersteps.UserSteps;

public class CommonPageJbehaveSteps {

	@Managed(driver = "firefox")
	public WebDriver driver;

	@Steps
	protected UserSteps user;

	@AfterStories
	public void closeDriver() {
		driver.close();
	}
}
