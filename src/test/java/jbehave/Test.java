package jbehave;

import net.thucydides.core.annotations.findby.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

public class Test {

	public static void main(String[] args) {
		System.out.println("Test Started");
			
		WebDriver driver = new FirefoxDriver();
		String baseUrl = "http://www.facebook.com";
		String tagName = "";
		driver.get(baseUrl);
		tagName = driver.findElement(By.id("email")).getTagName();
	    System.out.println(tagName);
		driver.close();
		System.exit(0);

	}
}
