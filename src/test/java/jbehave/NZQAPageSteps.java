package jbehave;

import jbehave.common.steps.CommonPageJbehaveSteps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.When;
import org.jbehave.core.annotations.Then;
import org.junit.Assert;

public class NZQAPageSteps extends CommonPageJbehaveSteps {

	private String returnInfo = "";
	private String returnTitle = "";

	@Given("\"NZQA HOME PAGE\"")
	public void givenNZQAHomePage() {
		user.openHomePage();
	}

	@When("I search for <info>")
	public void serachForText(String info) {
		returnInfo = user.checkForNZQAPageText();
	}

	@Then("I can see that the page contains the information <info>")
	public void checkForTitle(String info) {
		Assert.assertEquals("Information is not valid", info, returnInfo);
	}

	@Given("an \"NZQA OVERVIEW PAGE\" with qualnumber <number>")
	public void givenNZQAOverviewPage(String number) {
		user.openOverviewPage(number);
	}

	@When("I search for qualification title")
	public void serachForQualTitle() {
		returnTitle = user.checkForQualTitle();
	}

	@Then("I can see that the page contains title <title>")
	public void checkForQualTitle(String title) {
		Assert.assertEquals("Titles doesnt match", title, returnTitle);
	}

}
