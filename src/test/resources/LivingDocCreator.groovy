import org.pegdown.*
import groovy.xml.MarkupBuilder
import java.nio.charset.Charset

def inputDirName = project.properties['jbehaveOutDir']
def cssDir = project.properties['cssDir']
def outputDirName = inputDirName + '/spec'

createOutputDir(outputDirName)
writeCssToOutputDir(cssDir, outputDirName)
def featureFileList = enrichFeatureFiles(inputDirName, outputDirName)
createIndexPage(outputDirName, featureFileList)

private createOutputDir(String outputDirName) {
    println 'Creating output dir ' + outputDirName
    new File(outputDirName).deleteDir()
    new File(outputDirName).mkdir()
}

private writeCssToOutputDir(String baseCssDirName, String outputDirName) {
    println 'Writing css to ' + outputDirName
    new File(outputDirName, 'spec.css') << new File(baseCssDirName +
'/spec.css').text
}

/*
For each html file in inputDirName, replaces escape chars, adds CSS,
converts Markdown to HTML and writes to outputDirName.
Returns a list of features for that theme, with each feature containing a
map with keys success, fileName and title.

For example:
    [
      [title:'Feature One', fileName:'featureOne.html', success:true],
      [title:'Feature Two', fileName:'featureTwo.html', success:false]
    ]
*/
private List enrichFeatureFiles(String inputDirName, String outputDirName) {
    def featureList = []
    new File(inputDirName).eachFileMatch(~/.*\.html/) { featureFile ->
        if (featureFile.name != 'BeforeStories.html' && featureFile.name !=
'AfterStories.html' ) {
            def text = replaceEscapeCharacters(featureFile.text)
            def html = addCssTo(text)
            html = convertMarkdownToHtml(html)
            new File(outputDirName, featureFile.name).setText(html)
            featureList += summariseFeature(html, featureFile.name)
        }
    }
    return featureList
}

def convertMarkdownToHtml(String html) {
    return html.replaceAll(~"(?ms)/==(.*?)==/") { all, match ->
     def processor = new PegDownProcessor(Extensions.TABLES, 2000)
     return processor.markdownToHtml(match)
    }
}

def addCssTo(String html) {
    return """
<html>
 <head>
  <link rel='stylesheet' type='text/css' href='spec.css'>
 </head>
 <body>
  $html
 </body>
</html>
"""
}

def summariseFeature(String html, String fileName) {
    def success = !html.contains('<div class="step failed"')
    def featureName = html.find('(?ms)<h1>(.*)</h1>') { fullMatch, feature
->
        return feature.replace('&quot;', '"')
    }
    return [success:success, fileName:fileName,
title:replaceEscapeCharacters(featureName)]
}

def createIndexPage(String outputDirName, List featureList) {
    def writer = new BufferedWriter(new OutputStreamWriter(new
FileOutputStream(new File(outputDirName, "index.html")),
Charset.forName("UTF-8")))
    //def writer = new BufferedWriter(new StringWriter())
    def page = new MarkupBuilder(writer)
    def currentTheme = ''
    page.html() {
        head {
            link(rel:'stylesheet', type:'text/css', href:'spec.css')
        }
        body {
            h1('JBehav And Thucydides Demo Tests')
            h2('Features')
            def featuresByTheme = featureList.groupBy {
it.fileName.split('\\.')[1] }
            featuresByTheme.each { theme, features ->
                h2(theme)
                ul {
                    features.each { feature ->
                        System.out.println(feature.title)
                        li {
                            a(href:feature.fileName) {
                                span(class:(feature.success ? 'successful'
: 'failed') + ' spec') {
                                    mkp.yieldUnescaped(feature.title)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return writer
}

/**
 * This method will be used to revert a double escape performed by JBehave
in version 3.8
 * Hopefully in future versions with Apache Commons-Lang 3.x support we
will be able to remove
 * this.
 * @param text
 * @return
 */
def replaceEscapeCharacters(String text) {
    def out = text.replaceAll("&amp;#257;", "&#257;")
    out = out.replaceAll("&amp;#333;", "&#333;")
    out = out.replaceAll("&amp;eacute;" , "&eacute;")
    return out;
}