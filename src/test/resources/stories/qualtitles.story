Story1:Qualification Titles

Narrative:
In order to validate qualification titles
As a development team
I want to verify NZQA overview  page
					 
Scenario: Validate qualification titles
Given an "NZQA OVERVIEW PAGE" with qualnumber <number>
When I search for qualification title
Then I can see that the page contains title <title>
Examples:
|number|title|
|0001|National Certificate in Dairy Manufacturing (Technology) (Level 4)|